package com.example.aplikasiregistrasi.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Slf4j
@Controller
public class RegistrasiController {

    @GetMapping("/registrasi")
    public ModelMap displayFormRegistrasi(){
        log.info("Form Registrasi");
        ModelMap mm= new ModelMap();
        mm.addAttribute("nama", "Aww");
        return mm;
    }

    @PostMapping("/registrasi")
    public String prosesFormRegistrasi(){
        log.info("Seharusnya nanti simpan ke db");
        return "redirect:konfirmasi";
    }

    @GetMapping("/konfirmasi")
    public void displayFormKonfirmasi(){
        log.info("Form konfirmasi");
    }
}
