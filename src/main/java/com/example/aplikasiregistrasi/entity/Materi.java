package com.example.aplikasiregistrasi.entity;

import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.*;
import java.math.BigDecimal;


@Data
@Entity

public class Materi {
    @Id
    @GeneratedValue (generator = "uuid")
    @GenericGenerator(name ="uuid", strategy ="uuid2")
    private String id;

    @NotEmpty @Size(min = 3,max = 100)
    private String kode;

    @NotEmpty @Size(min=2, max=100)
    private String nama;

    @NotNull @Min(10000)
    private BigDecimal biaya;

}
